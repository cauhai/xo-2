function allBooks(db) {
  return new Promise((resolve, reject) => {
    db.collection('books', (err, collection) => {
      collection.find().toArray(function (err, data) {
        if (err) reject(err);
        resolve(data);
      });
    });
  });
}

module.exports = { allBooks }