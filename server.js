const express = require('express');
const app = express();
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const dbURI = 'mongodb+srv://mongco:mongco@cluster0-mf6gb.mongodb.net/xodb';
const hostname = '0.0.0.0';
const port = 8080;
const bookdb = require('./service/bookdb');

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end('<h1>Hello from XO-2!</h1>');
});

app.get('/about', (req, res) => {
  var now = new Date();
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end(`<h1>About XO-2!</h1> ${now}`);
  // TODO read from about.html
});

router.get('/status', (req, res) => {
  if (app.locals.db) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text');
    res.end('OK');
  } else {
    res.statusCode = 503;
    res.setHeader('Content-Type', 'text');
    res.end('Database not reachable.');
  }
});

router.get('/books', (req, res) => {
  bookdb.allBooks(app.locals.db).then((data) => {
    res.json(data);
  }).catch(err => {
    console.log(err);
    res.end('<h2>There was some server error. Please read log.</h2>');
  });
});

router.get('/book/:id', (req, res) => {
  res.end(`Data for book with id=${req.params.id}`);
  // TODO retrieve book
});

// TODO GET search
// TODO POST create, edit, delete

app.use('/', router);

MongoClient.connect(dbURI, (err, dbClient) => {
  if (err) {
    console.log(`Failed to connect to database. ${err.stack}`);
  }
  app.locals.db = dbClient.db('xodb');
  app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });
});

